use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::BufReader;
use std::path::Path;

#[derive(Serialize, Deserialize)]
pub struct Config {
    pub host_name: String,
    pub port: i32,
}

impl Config {
    //Loads from file path
    pub fn load_from_path(file_path: &Path) -> Result<Self, String> {
        if let Ok(file) = File::open(file_path) {
            let reader = BufReader::new(file);
            if let Ok(config) = serde_json::from_reader(reader) {
                Ok(config)
            } else {
                Err(format!(
                    "Failed to deserialize config located at {:?}",
                    file_path
                ))
            }
        } else {
            Err(format!(
                "Failed to find open file located at {:?}!",
                file_path
            ))
        }
    }

    pub fn get_bind_string(&self) -> String {
        let mut bind_str = String::default();
        bind_str.push_str(&self.host_name);
        bind_str.push_str(":");
        bind_str.push_str(&self.port.to_string());
        bind_str
    }
}

#[test]
fn test_bind_string_in_config() {
    let config = Config {
        host_name: "127.0.0.1".to_string(),
        port: 8080,
    };
    assert_eq!("127.0.0.1:8080".to_string(), config.get_bind_string());
}
