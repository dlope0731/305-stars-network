extern crate diesel;
extern crate dotenv;

use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};
use dotenv::dotenv;
use std::env;

pub type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;

//Creates the database pool using PostgreSQL backend. Panics if it cannot create pool.
pub fn create_db_pool() -> Pool {
    dotenv().ok();

    if let Ok(database_url) = env::var("DATABASE_URL") {
        let manager = ConnectionManager::<PgConnection>::new(database_url);
        r2d2::Pool::builder()
            .build(manager)
            .expect("Failed to create database pool.")
    } else {
        panic!("Failed to find 'DATABASE_URL' environment variable".to_string());
    }
}
