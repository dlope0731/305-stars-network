use crate::db_connection::Pool;
use crate::diesel::RunQueryDsl;
use crate::models::songs::get_song_list;
use crate::models::songs::Song;
use crate::schema::highscores;
use actix_identity::Identity;
use actix_session::Session;
#[allow(unused_imports)]
use actix_web::http::{header, StatusCode};
use actix_web::Result;
use actix_web::{web, HttpRequest, HttpResponse, Responder};
use diesel::prelude::*;
use diesel::sql_query;
#[allow(unused_imports)]
use diesel::sql_types::SmallInt;
#[allow(unused_imports)]
use diesel::sql_types::Text;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Insertable)]
#[table_name = "highscores"]
pub struct HighScoreEntry {
    pub song_hash: String,
    pub dancer_tag: String,
    pub ex_score: i16,
    pub judgment: i16,
    pub created_at: chrono::NaiveDateTime,
}

impl HighScoreEntry {
    pub fn publish(&self, pg_connection: &PgConnection) -> bool {
        if self.ex_score == 0 || self.judgment < 4 {
            return false; // We dont'publish trash results sorry
        }

        use crate::schema::highscores::dsl::*;
        if let Ok(entry) = highscores
            .filter(dancer_tag.eq(self.dancer_tag.clone()))
            .filter(song_hash.eq(self.song_hash.clone()))
            .filter(judgment.eq(self.judgment))
            .first::<HighScoreQuery>(pg_connection)
        {
            if self.ex_score > entry.ex_score {
                // We only want to update the highscore entry if it is a new better ex score for that song and judgment
                diesel::update(&entry)
                    .set(ex_score.eq(self.ex_score))
                    .execute(pg_connection)
                    .is_ok()
            } else {
                false
            }
        } else {
            diesel::insert_into(highscores)
                .values(self)
                .get_results::<HighScoreQuery>(pg_connection)
                .is_ok()
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Identifiable, Queryable, QueryableByName)]
#[table_name = "highscores"]
pub struct HighScoreQuery {
    pub id: i32,
    pub song_hash: String,
    pub dancer_tag: String,
    pub ex_score: i16,
    pub judgment: i16,
    pub created_at: chrono::NaiveDateTime,
}

impl HighScoreQuery {
    pub fn as_html_table_row(&self) -> String {
        format!(
            "<tr><td>{}</td><td>{}</td></tr>",
            self.dancer_tag, self.ex_score
        )
    }

    pub fn as_html_table_top_row(&self, songs: &Vec<Song>) -> String {

        for song in songs {
            if song.id == self.song_hash {
                return format!(
                    "<tr><td><a href='/highscore/{0}/{3}'>{1}</a></td><td>{2}</td><td>{3}</td></tr>",
                    self.song_hash, song.title, self.ex_score, self.judgment
                );
            }
        }

        return String::default();
    }
}

fn get_top_scores_by_user(dancer_tag: String, connection: &PgConnection) -> Vec<HighScoreQuery> {
    sql_query(format!(
        "SELECT * FROM highscores WHERE dancer_tag='{}' ORDER BY ex_score DESC",
        dancer_tag
    ))
    .load(connection)
    .unwrap_or(Vec::<HighScoreQuery>::default())
}

#[derive(Debug, Serialize, Deserialize, Queryable, QueryableByName)]
pub struct MVPQuery {
    #[sql_type = "Text"]
    pub dancer_tag: String,
    #[sql_type = "SmallInt"]
    pub sum: i16,
}

fn get_305_network_mvp(connection: &PgConnection) -> String {
    let result: std::result::Result<Vec<MVPQuery>, diesel::result::Error> = sql_query(
        "SELECT dancer_tag, sum(ex_score) FROM highscores GROUP BY dancer_tag ORDER BY sum DESC",
    )
    .load(connection);

    match result {
        Ok(dancers) => {
            return dancers[0].dancer_tag.clone();
        }
        Err(_e) => {
            println!("{:?}", _e);
            return String::from("No User");
        }
    }
}

#[get("/highscore")]
pub async fn get(
    _session: Session,
    _request: HttpRequest,
    pool: web::Data<Pool>,
) -> Result<HttpResponse> {
    let mut song_list_html = String::default();
    let mut mvp = String::default();
    if let Ok(connection) = &pool.get() {
        let songs = get_song_list(connection);
        mvp = get_305_network_mvp(connection);
        if songs.is_empty() {
            song_list_html.push_str("<div><b>No Songs in the database</b></div>");
        } else {
            song_list_html.push_str(
                "<div><b>The songs are included in a fork of stepmania that can connect to the 305 stars network. Here is the link for the <a href='https://drive.google.com/file/d/1joXfvVqFKWD04lmyVXtSOx6pLTuccCUY/view?usp=sharing'>client</a>.</b> This has the Windows version pre-built. Refer to the Linux build guide for stepmania.</div>",
            );
            song_list_html.push_str("");
        }

        for song in songs {
            song_list_html.push_str(song.generate_html_link().as_str());
        }
    }

    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(format!(
            include_str!("../static/highscore.html"),
            mvp, song_list_html
        )))
}

pub async fn get_specific_scores(
    _session: Session,
    request: HttpRequest,
    pool: web::Data<Pool>,
) -> Result<HttpResponse> {
    let mut rows = String::default();
    let mut numerical_judgment = 0;
    let mut song_name = String::default();
    if let Some(shash) = request.match_info().get("song_hash") {
        if let Some(j) = request.match_info().get("judgment") {
            numerical_judgment = j.to_string().parse::<i16>().unwrap_or(0);
            use crate::schema::highscores::dsl::*;
            if let Ok(connection) = &pool.get() {
                //Fetch all the scores based off judgment and song it will be sorted by ex_score (largest to smallest)
                let result = highscores
                    .filter(song_hash.eq(shash.clone()))
                    .filter(judgment.eq(numerical_judgment))
                    .order(ex_score.desc())
                    .limit(100) // We want to show top 100
                    .load(connection)
                    .unwrap_or(Vec::<HighScoreQuery>::default());
                for entry in result {
                    rows.push_str(entry.as_html_table_row().as_str());
                }
                //Get song name based off the hash
                use crate::schema::songs::dsl::*;
                if let Ok(song) = songs.filter(id.eq(shash)).first::<Song>(connection) {
                    song_name = song.title.clone();
                }
            }
        }
    }

    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(format!(
            include_str!("../static/leaderboard.html"),
            song_name, numerical_judgment, rows
        )))
}

pub async fn get_profile(
    _session: Session,
    request: HttpRequest,
    pool: web::Data<Pool>,
) -> Result<HttpResponse> {
    let mut rows = String::default();
    let username : String;
    let mut total_ex_score : u64 = 0;
    if let Some(dancer_tag) = request.match_info().get("dancer_tag") {
        if let Ok(connection) = &pool.get() {
            let scores = get_top_scores_by_user(dancer_tag.to_string(), connection);
            if scores.len() > 0 {
                username = dancer_tag.to_string();
                let songs = get_song_list(connection);
                for score in scores {
                    total_ex_score = total_ex_score + (score.ex_score as u64);
                    rows.push_str(score.as_html_table_top_row(&songs).as_str());
                }
            } else {
                return HttpResponse::build(StatusCode::BAD_REQUEST).await;
            }
        } else {
            return HttpResponse::build(StatusCode::BAD_REQUEST).await;
        }
    } else {
        return HttpResponse::build(StatusCode::BAD_REQUEST).await;
    }

    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(format!(
            include_str!("../static/profile.html"),
            username, total_ex_score, rows
        )))
}

#[derive(Serialize, Deserialize)]
pub struct HighScoreEntryForm {
    pub song_hash: String,
    pub dancer_tag: String,
    pub ex_score: String,
    pub judgment: String,
}

impl HighScoreEntryForm {
    pub fn to_entry(&self) -> HighScoreEntry {
        HighScoreEntry {
            song_hash: self.song_hash.clone(),
            dancer_tag: self.dancer_tag.clone(),
            ex_score: self.ex_score.parse::<i16>().unwrap_or(0),
            judgment: self.judgment.parse::<i16>().unwrap_or(0),
            created_at: chrono::Local::now().naive_local(),
        }
    }
}

pub async fn post(
    _request: HttpRequest,
    _id: Identity, // Note: We need to use this confirm idenity of the incoming highscore submission
    form: web::Form<HighScoreEntryForm>,
    pool: web::Data<Pool>,
) -> impl Responder {
    if let Ok(connection) = &pool.get() {
        if form.to_entry().publish(connection) {
            HttpResponse::build(StatusCode::OK)
        } else {
            HttpResponse::build(StatusCode::BAD_REQUEST)
        }
    } else {
        HttpResponse::build(StatusCode::BAD_REQUEST)
    }
}
