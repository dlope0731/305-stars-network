use crate::db_connection::Pool;
use crate::models::users::register_user;
use actix_identity::Identity;
use actix_session::Session;
use actix_web::http::{header, StatusCode};
use actix_web::Result;
use actix_web::{web, HttpRequest, HttpResponse, Responder};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct RegisteredUser {
    pub dancer_tag: String,
    pub password: String,
    pub confirmed_password: String,
}

impl RegisteredUser {
    // Returns true if password is confirmed i.e. both password fields match this is to ensure user wants that specific password without typo
    pub fn password_confirmed(&self) -> bool {
        self.password == self.confirmed_password
    }
}

#[get("/register")]
pub async fn get(_session: Session, _request: HttpRequest) -> Result<HttpResponse> {
    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(format!(include_str!("../static/register.html"), "")))
}

pub async fn post(
    _request: HttpRequest,
    id: Identity,
    form: web::Form<RegisteredUser>,
    pool: web::Data<Pool>,
) -> impl Responder {
    if let Ok(connection) = &pool.get() {
        if form.password_confirmed()
            && register_user(form.dancer_tag.clone(), form.password.clone(), connection).is_ok()
        {
            id.remember(form.dancer_tag.clone());
            HttpResponse::Found()
                .set_header(header::LOCATION, "/")
                .finish()
        } else {
            HttpResponse::build(StatusCode::OK)
                .content_type("text/html; charset=utf-8")
                .body(format!(
                    include_str!("../static/register.html"),
                    "User already registered!"
                ))
        }
    } else {
        HttpResponse::build(StatusCode::OK)
            .content_type("text/html; charset=utf-8")
            .body(format!(
                include_str!("../static/register.html"),
                "Failed to register user!"
            ))
    }
}

#[test]
fn test_password_confirmed() {
    let user = RegisteredUser {
        dancer_tag: String::default(),
        password: "1234".to_string(),
        confirmed_password: "1234".to_string(),
    };
    assert_eq!(user.password_confirmed(), true);
    let user = RegisteredUser {
        dancer_tag: String::default(),
        password: "1234".to_string(),
        confirmed_password: "12345".to_string(),
    };
    assert_eq!(user.password_confirmed(), false);
}
