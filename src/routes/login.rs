use crate::db_connection::Pool;
use crate::models::users::User;
use crate::schema::users::dsl::*;
use actix_identity::Identity;
use actix_session::Session;
#[allow(unused_imports)]
use actix_web::http::{header, StatusCode};
use actix_web::Result;
use actix_web::{web, HttpRequest, HttpResponse, Responder};
use bcrypt::verify;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct LoginUser {
    pub dancer_tag: String,
    pub password: String,
}

impl LoginUser {
    pub fn password_verified(&self, hashed_password: String) -> bool {
        verify(self.password.clone(), hashed_password.as_str()).unwrap_or(false)
    }
}

#[get("/login")]
pub async fn get(_session: Session, _request: HttpRequest) -> Result<HttpResponse> {
    Ok(HttpResponse::build(StatusCode::OK)
        .content_type("text/html; charset=utf-8")
        .body(format!(include_str!("../static/login.html"), "")))
}

pub async fn post(
    _request: HttpRequest,
    id: Identity,
    form: web::Form<LoginUser>,
    pool: web::Data<Pool>,
) -> impl Responder {
    if let Ok(connection) = &pool.get() {
        let result = users
            .filter(dancer_tag.eq(form.dancer_tag.clone()))
            .first::<User>(connection);
        if let Ok(user) = result {
            if form.password_verified(user.get_hash_password()) {
                id.remember(form.dancer_tag.clone());
                HttpResponse::Found()
                    .set_header(header::LOCATION, "/")
                    .finish()
            } else {
                HttpResponse::build(StatusCode::UNAUTHORIZED)
                    .content_type("text/html; charset=utf-8")
                    .body(format!(
                        include_str!("../static/login.html"),
                        "Invalid password or unknown user!"
                    ))
            }
        } else {
            HttpResponse::build(StatusCode::UNAUTHORIZED)
                .content_type("text/html; charset=utf-8")
                .body(format!(
                    include_str!("../static/login.html"),
                    "Invalid password or unknown user!"
                ))
        }
    } else {
        HttpResponse::build(StatusCode::UNAUTHORIZED)
            .content_type("text/html; charset=utf-8")
            .body(format!(
                include_str!("../static/login.html"),
                "Database Error!"
            ))
    }
}
