use actix_identity::Identity;
use actix_web::http::StatusCode;
use actix_web::Result;
use actix_web::{HttpRequest, HttpResponse};

#[get("/")]
pub async fn get(id: Identity, _request: HttpRequest) -> Result<HttpResponse> {
    match id.identity() {
        Some(username) => Ok(HttpResponse::build(StatusCode::OK)
            .content_type("text/html; charset=utf-8")
            .body(format!(
                include_str!("../static/index.html"),
                format!("<a href='/profile/{0}'>Welcome {0}!</a>", username)
            ))),
        None => Ok(HttpResponse::build(StatusCode::OK)
            .content_type("text/html; charset=utf-8")
            .body(format!(
                include_str!("../static/index.html"),
                "<a href=\"/login\">Login</a>"
            ))),
    }
}
