#[macro_use]
extern crate actix_web;
#[macro_use]
extern crate diesel;
extern crate serde;
extern crate serde_json;

mod config;
mod db_connection;
mod models;
mod routes;
mod schema;

use actix_identity::{CookieIdentityPolicy, IdentityService};
use actix_web::{middleware, web, App, HttpServer};
use config::Config;
use db_connection::{create_db_pool, Pool};
use std::env::current_dir;

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    let pool = create_db_pool(); // Fetch database connection pool function will panic if something is wrong and terminate program
    let mut current_dir_buf = current_dir().unwrap();
    current_dir_buf.push("config.json"); // config.json should be located in the default working directory -- this can be changed later in the future
    let server_future = match Config::load_from_path(current_dir_buf.as_path()) {
        // Load the json config into our Config struct let's match to an OK or ERROR
        Ok(config) => start_server(config, pool), // If OK get config wrapped in Ok and start the HTTP server
        Err(error_message) => panic!(error_message), // Else get the string wrapped in the Err and panic with the error message (Note: panic will terminate process as no config no way of starting)
    };
    server_future.await
}

async fn start_server(config: Config, pool: Pool) -> std::io::Result<()> {
    println!(
        "Starting StepMania Stars 305 Network Service ({})...",
        config.get_bind_string()
    );
    std::env::set_var("RUST_LOG", "actix_web=info");
    env_logger::init();
    HttpServer::new(move || {
        App::new()
            .data(pool.clone()) // Provide the database pool
            .wrap(IdentityService::new(
                CookieIdentityPolicy::new(&[0; 32])
                    .name("305-stars-network")
                    .secure(false), // TODO: Setup SSL for HTTPS or else risk MITM
            ))
            .wrap(middleware::Logger::default())
            .service(routes::index::get) // Index route GET / returns index HTML document
            .service(routes::login::get) // (Login GET) returns login HTML form
            .service(web::resource("/login").route(web::post().to(routes::login::post))) // (Login POST) login user given their password
            .service(routes::register::get) // (Register GET) returns account registration HTML form
            .service(web::resource("/register").route(web::post().to(routes::register::post))) // (Register POST) sends user registration form via HTTP POST
            .service(routes::highscore::get)
            .service(
                web::resource("/highscore/{song_hash}/{judgment}")
                    .route(web::get().to(routes::highscore::get_specific_scores)),
            )
            .service(
                 web::resource("/profile/{dancer_tag}").route(web::get().to(routes::highscore::get_profile)),
            )
            .service(web::resource("/highscore").route(web::post().to(routes::highscore::post)))
    })
    .bind(config.get_bind_string())?
    .run()
    .await
}
