table! {
    highscores (id) {
        id -> Int4,
        song_hash -> Varchar,
        dancer_tag -> Varchar,
        ex_score -> Int2,
        judgment -> Int2,
        created_at -> Timestamp,
    }
}

table! {
    songs (id) {
        id -> Varchar,
        title -> Varchar,
        created_at -> Timestamp,
    }
}

table! {
    users (dancer_tag) {
        dancer_tag -> Varchar,
        password -> Varchar,
        active -> Bool,
        created_at -> Timestamp,
    }
}

allow_tables_to_appear_in_same_query!(highscores, songs, users,);
