use crate::diesel::RunQueryDsl;
use crate::schema::songs;
use diesel::PgConnection;
use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "songs"]
pub struct Song {
    pub id: String,
    pub title: String,
    pub created_at: chrono::NaiveDateTime,
}

impl Song {
    pub fn generate_html_link(&self) -> String {
        format!("<div><p>Title: {0}</p> <a href=\"highscore/{1}/4\">Judge 4 Highscores</a> <a href=\"highscore/{1}/5\">Judge 5 Highscores</a> <a href=\"highscore/{1}/6\">Judge 6 Highscores</a></div><br>", self.title, self.id)
    }
}

pub fn get_song_list(connection: &PgConnection) -> Vec<Song> {
    use crate::schema::songs::dsl::*;
    songs.load(connection).unwrap_or(Vec::<Song>::default())
}
