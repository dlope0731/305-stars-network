use crate::diesel::RunQueryDsl;
use crate::schema::users;
use bcrypt::{hash, DEFAULT_COST};
use diesel::{insert_into, PgConnection};
use serde::{Deserialize, Serialize};
use std::result::Result;

#[derive(Debug, Serialize, Deserialize, Queryable, Insertable)]
#[table_name = "users"]
pub struct User {
    // Represents a row of the users table refer to schema.rs and migration script to get an understanding.
    pub dancer_tag: String,
    #[serde(skip)]
    // Tell the serde serializer to skip or else we end up showing our password as a json for example
    pub password: String,
    pub active: bool, // Used to support banning a user or deactivating user
    pub created_at: chrono::NaiveDateTime,
}

impl User {
    pub fn new(dancer_tag: String, password: String) -> Result<Self, String> {
        if let Ok(hashed_password) = hash(password, DEFAULT_COST) {
            Ok(User {
                dancer_tag: dancer_tag,
                password: hashed_password,
                active: true,
                created_at: chrono::Local::now().naive_local(),
            })
        } else {
            Err(format!(
                "Failed to hash password for user: '{}'",
                dancer_tag
            ))
        }
    }

    pub fn get_hash_password(&self) -> String {
        self.password.clone()
    }
}

pub fn register_user(
    dancer_tag: String,
    password: String,
    pg_connection: &PgConnection,
) -> Result<(), String> {
    match User::new(dancer_tag, password) {
        Ok(user) => {
            if let Err(e) = insert_into(users::table)
                .values(user)
                .get_results::<User>(pg_connection)
            {
                Err(format!("Error: {:?}", e))
            } else {
                Ok(())
            }
        }
        Err(error_message) => Err(error_message),
    }
}
