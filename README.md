# Stepmania Leaderboard Server

## About

This project is a free and open source project aimed at developing an online network for stepmania. We aim in the beginng to provide a leaderboard for each DDR Mix and ITG release from 1-3.
The leaderboard will be split by song and judgment (starting at 5). This project will also modify Stepmania so that it supports authenticating in the network.

## Docker Container
This project has a way to build a docker image that builds and run the code from master in release mode. Instructions are simple in the root directory of the repo run the following commands:

**Build and run the image:**
`docker -Htcp://127.0.0.1:2375 build . -t sm305-network --network host`

## Future Goals

If we can get the basics out of the way we hope to aim for things like versus mode, seasonal rewards for being top of the leaderboard (rewards cosmetic like noteskins), etc.
