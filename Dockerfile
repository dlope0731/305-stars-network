FROM rust:latest as base
LABEL Name=sm305_network Version=0.0.1
#Update apt
RUN apt update
#Install git needed to get the source code to build
RUN apt-get install -y git
#Clone repo
RUN git clone https://gitlab.com/dlope0731/305-stars-network.git
#Go inside repo directory
WORKDIR /305-stars-network
#Create env file -- assumes it will have access to host network as the DB server will not be running inside container
RUN echo "DATABASE_URL=postgres://postgres:subrutina@localhost/305_stars" > .env
#Set host and port config
RUN echo "{\"host_name\" : \"0.0.0.0\",\"port\" : 80}" > config.json
#Install ORM depedency
RUN cargo install diesel_cli --no-default-features --features postgres
#Setup database
RUN diesel setup
#Run migrations
RUN diesel migration run
#Build and run from source with release mode for most optimized compilation
RUN cargo run --release
